#!/bin/sh

PACKAGE=htmlunit-core-js
SRC_VERSION=$2
TARBALL=../${PACKAGE}_${SRC_VERSION}.orig.tar.gz

rm -rf get-orig-source $TARBALL
mkdir get-orig-source
svn export https://htmlunit.svn.sourceforge.net/svnroot/htmlunit/tags/core-js-${SRC_VERSION} \
    get-orig-source/${PACKAGE}-${SRC_VERSION}
# remove CVS metadata
find get-orig-source/ -depth -name CVS -exec rm -r {} \;
find get-orig-source/ -name .cvsignore -exec rm  {} \;
# remove binary libraries
rm get-orig-source/${PACKAGE}-${SRC_VERSION}/lib/*
# remove non DFSG-compatible but not needed files
rm -r get-orig-source/${PACKAGE}-${SRC_VERSION}/rhino/toolsrc/org/mozilla/javascript/tools/debugger/downloaded
GZIP=--best tar czf $TARBALL -C get-orig-source ${PACKAGE}-${SRC_VERSION}
rm -rf get-orig-source
echo "  "$TARBALL" created"
